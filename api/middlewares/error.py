import traceback
from django.core.exceptions import ValidationError
from rest_framework import exceptions as rest_exceptions
from rest_framework.views import Response

from ..errors import errors
from ..errors.my_error import MyError

error_file = open('errors.log', 'a')


def error_handler(ex, context):
    if type(ex) is MyError:
        return Response({
            'error': {
                'code': ex.info['error']['code'],
                'message': ex.info['error']['message'],
                'userMessage': ex.info['error']['userMessage'],
            }
        }, status=ex.info['status'])
    elif type(ex) is ValidationError:
        print(ex)
        message = ""
        for item in ex:
            message = message + item[1][0] + "\n"
        return Response({
            'error': {
                'code': errors.API_INVALID_INPUT['error']['code'],
                'message': message,
                'userMessage': errors.API_INVALID_INPUT['error']['userMessage']
            }
        }, status=errors.API_INVALID_INPUT['status'])
    elif type(ex) is rest_exceptions.MethodNotAllowed:
        return Response({
            'error': {
                'code': errors.SERVER_METHOD_NOT_ALLOWED['error']['code'],
                'message': errors.SERVER_METHOD_NOT_ALLOWED['error']['message'],
                'userMessage': errors.SERVER_METHOD_NOT_ALLOWED['error']['userMessage'],
            }
        }, status=errors.SERVER_METHOD_NOT_ALLOWED['status'])
    else:
        error_file.write(f'url ---> {context["request"]}\n')
        error_file.write(f'url params ---> {context["kwargs"]}\n')
        error_file.write(f'query params ---> {context["request"].query_params}\n')
        error_file.write(f'request body ---> {context["request"].data}\n')
        error_file.write(f'exception ---> {ex}\n')
        error_file.write('traceback ---> \n')
        traceback.print_exc(file=error_file)
        error_file.write('-------------------------------------\n')
        error_file.flush()
        return Response({
            'error': {
                'code': errors.SERVER_INTERNAL_ERROR['error']['code'],
                'message': errors.SERVER_INTERNAL_ERROR['error']['message'],
                'userMessage': errors.SERVER_INTERNAL_ERROR['error']['userMessage'],
            }
        }, status=errors.SERVER_INTERNAL_ERROR['status'])
