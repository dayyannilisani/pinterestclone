from apps.user.services.token import validate_access_token


class AuthMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if 'Authorization' in request.headers:
            token = request.headers['Authorization'][7:]
            output = validate_access_token(token)

            request.user_id = output['id']
            request.role = output['role']
            request.is_logged_in = True
        else:
            request.is_logged_in = False

        response = self.get_response(request)
        return response
