from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from ..models import User
from ..services.password import hash_password
from ..serializers.outputs import UserOut
from api.utils import checks
from api.utils.services import get_user_by_id, update_item, create_item


class CreateUserView(APIView):
    def post(self, request):
        request.data.pop('role', None)
        request.data.pop('id', None)
        request.data['password'] = hash_password(request.data)
        user = create_item(User, request.data)
        result = UserOut(user)
        return Response(result.data, status=status.HTTP_201_CREATED)

    @checks.login
    def get(self, request):
        user = get_user_by_id(request.user_id)
        response = UserOut(user)
        return Response(response.data, status=status.HTTP_200_OK)

    @checks.login
    def delete(self, request):
        user = get_user_by_id(request.user_id)
        user.delete()
        return Response(status=status.HTTP_200_OK)

    @checks.login
    def put(self, request):
        request.data.pop('role', None)
        request.data.pop('password', None)
        user = get_user_by_id(request.user_id)
        updated_user = update_item(user, request.data)
        response = UserOut(updated_user)
        return Response(response.data, status=status.HTTP_200_OK)
