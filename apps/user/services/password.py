from django.contrib.auth.hashers import make_password
from api.errors import errors
from api.errors.my_error import MyError


def hash_password(data):
    if 'password' not in data.keys() or data['password'] == '' or data['password'] is None:
        raise MyError(errors.API_INVALID_INPUT, "رمز عبور شما نمی تواند خالی باشد")

    return make_password(data['password'])
