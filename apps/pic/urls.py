from django.urls import path
from .views import CreatePic, GetPics, GetPic

urlpatterns = [
    path('', CreatePic.as_view()),
    path('<int:offset>/<int:limit>/', GetPics.as_view()),
    path('<int:pic_id>/', GetPic.as_view())
]
