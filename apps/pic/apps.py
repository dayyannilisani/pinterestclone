from django.apps import AppConfig


class PicConfig(AppConfig):
    name = 'apps.pic'
