from rest_framework import serializers
from .models import Pic
from apps.tag.serializers import TagOut


class PicOut(serializers.ModelSerializer):
    tags = TagOut(many=True)

    class Meta:
        model = Pic
        fields = '__all__'
