from os import remove
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import FileResponse

from api.utils import checks
from api.utils.services import create_item, get_item_by_id
from api.errors.my_error import MyError
from api.errors import errors

from .models import Pic
from .serializers import PicOut


class CreatePic(APIView):
    @checks.login
    def post(self, request):
        request.data['user_id'] = request.user_id
        tags = request.data['tags'].split(',')
        pic = create_item(Pic, request.data)
        for tag in tags:
            pic.tags.add(int(tag))
        return Response(status=status.HTTP_201_CREATED)

    @checks.login
    def get(self, request):
        pics = Pic.objects.filter(user_id=request.user_id)
        response = PicOut(pics, many=True)
        return Response(response.data, status=status.HTTP_200_OK)


class GetPics(APIView):
    def get(self, request, offset, limit):
        pics = Pic.objects.filter(private=False)[offset:limit]
        response = PicOut(pics, many=True)
        return Response(response.data, status=status.HTTP_200_OK)


class GetPic(APIView):
    def get(self, request, pic_id):
        pic = get_item_by_id(Pic, pic_id)
        if not pic.private:
            pic = open('media/' + str(pic.pic), 'rb')
            response = FileResponse(pic)
            return response
        else:
            if hasattr(request, 'user_id') and request.user_id == pic.user_id:
                pic = open('media/' + str(pic.pic), 'rb')
                response = FileResponse(pic)
                return response
            else:
                raise MyError(errors.API_ADMIN_REQUIRED)

    @checks.login
    def delete(self, request, pic_id):
        pic = get_item_by_id(Pic, pic_id)
        if request.role != 'admin' or request.user_id != pic.user_id:
            raise MyError(errors.API_ADMIN_REQUIRED)

        remove('media/' + str(pic.pic))
        pic.delete()
        return Response(status=status.HTTP_200_OK)
