from django.db import models
from apps.tag.models import Tag
from apps.user.models import User
from api.utils.services import get_random_string
from api.utils.validation import generate_validation_message


def pic_path(self, file):
    return f'{self.title} {get_random_string(8)}.jpg'


# Create your models here.
class Pic(models.Model):
    title = models.CharField(max_length=500, null=False, blank=False,
                             error_messages=generate_validation_message('عنوان'))
    description = models.TextField(null=True, blank=True)
    private = models.BooleanField(default=False)
    pic = models.ImageField(upload_to=pic_path, error_messages=generate_validation_message('عکس'))
    tags = models.ManyToManyField(Tag)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def __repr__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.full_clean()
        super(Pic, self).save(*args, **kwargs)
