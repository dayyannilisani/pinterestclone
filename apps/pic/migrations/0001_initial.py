# Generated by Django 3.1.7 on 2021-02-26 15:25

import apps.pic.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('user', '0001_initial'),
        ('tag', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pic',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(error_messages={'blank': 'عنوان شما نمی تواند خالی باشد', 'invalid': 'عنوان انتخابی شما مناسب نمی باشد', 'null': 'عنوان شما نمی تواند خالی باشد', 'required': 'عنوان شما نمی تواند خالی باشد', 'unique': 'عنوان انتخابی شما از قبل مورد استفاده قرار گرفته است'}, max_length=500)),
                ('description', models.TextField(blank=True, null=True)),
                ('private', models.BooleanField(default=False)),
                ('pic', models.ImageField(error_messages={'blank': 'عکس شما نمی تواند خالی باشد', 'invalid': 'عکس انتخابی شما مناسب نمی باشد', 'null': 'عکس شما نمی تواند خالی باشد', 'required': 'عکس شما نمی تواند خالی باشد', 'unique': 'عکس انتخابی شما از قبل مورد استفاده قرار گرفته است'}, upload_to=apps.pic.models.pic_path)),
                ('tags', models.ManyToManyField(to='tag.Tag')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='user.user')),
            ],
        ),
    ]
