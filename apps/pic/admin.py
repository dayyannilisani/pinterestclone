from django.contrib import admin
from .models import Pic


# Register your models here.
@admin.register(Pic)
class PicAdmin(admin.ModelAdmin):
    pass
