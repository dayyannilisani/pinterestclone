from django.urls import path
from .views import ExceptDeleteOperationsForTag, DeleteTag


urlpatterns = [
    path('', ExceptDeleteOperationsForTag.as_view()),
    path('<str:tags>/', DeleteTag.as_view()),
]
