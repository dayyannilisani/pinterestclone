from django.db import transaction
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Tag
from .serializers import TagOut
from api.utils import checks
from api.utils.services import bulk_create_items, bulk_update_items


class ExceptDeleteOperationsForTag(APIView):
    @checks.admin
    def post(self, request):
        tags = bulk_create_items(Tag, request.data)
        response = TagOut(tags, many=True)
        return Response(response.data, status=status.HTTP_201_CREATED)

    @checks.admin
    def put(self, request):
        tags = bulk_update_items(Tag, request.data)
        response = TagOut(tags, many=True)
        return Response(response.data, status=status.HTTP_200_OK)

    def get(self, request):
        tags = Tag.objects.all()
        response = TagOut(tags, many=True)
        return Response(response.data, status=status.HTTP_200_OK)


class DeleteTag(APIView):
    @checks.admin
    @transaction.atomic
    def delete(self, request, tags: str):
        ids = tags.split(',')
        tags = Tag.objects.filter(pk__in=ids)
        for tag in tags:
            tag.delete()

        return Response(status=status.HTTP_200_OK)
