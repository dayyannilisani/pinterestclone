from django.db import models
from api.utils.validation import generate_validation_message


# Create your models here.
class Tag(models.Model):
    title = models.CharField(max_length=200, blank=False, unique=True,
                             error_messages=generate_validation_message('عنوان'))

    def __str__(self):
        return self.title

    def __repr__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.full_clean()
        super(Tag, self).save(*args, **kwargs)
